package com.example.nurilmiadmin;


import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)

    public class AddDeskripsiTest {
        String deskripsi="Rukuk merupakan bagian dari salat yang berarti membungkukkan badan hingga jari-jarinya menyentuh lutut.";


        @Rule
        public ActivityTestRule<Deskripsi> activityRule = new ActivityTestRule<Deskripsi>(Deskripsi.class);

        @Test
        public void checkGagal() throws Exception {
            onView(withId(R.id.et_deskripsi)).perform(typeText(deskripsi), closeSoftKeyboard());
            onView(withId(R.id.btn_add)).perform(click());
        }


    public void checkBerhasil() throws Exception {
            onView(withId(R.id.et_deskripsi)).perform(typeText(deskripsi), closeSoftKeyboard());
            onView(withId(R.id.btn_add)).perform(click());
        }
    }

