package com.example.nurilmiadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class Alasan extends AppCompatActivity {
    EditText isiAlasan, edt_tanggal, edt_waktu;;
    FirebaseDatabase database;
    DatabaseReference myRef;
    String alasan, tanggal, waktu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alasan);
        Intent intent = getIntent();
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("jadwal");
        isiAlasan = findViewById(R.id.editText);
        alasan = intent.getStringExtra("alasan");
    }

    public void back(View view) {
        ImageButton back = findViewById(R.id.imageButton14);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Alasan.this,
                        PilihMatpel.class);
                startActivity(intent);
            }
        });
    }

    public void home(View view) {
        ImageButton home = findViewById(R.id.imageButton10);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Alasan.this,
                        MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void tambahkan(View view) {
        String alasan = isiAlasan.getText().toString();
        if (!TextUtils.isEmpty(alasan)){
            Toast.makeText(this,"Data Berhasil Masuk",Toast.LENGTH_SHORT).show();
            IsiAlasan isialasan = new IsiAlasan(alasan);
            String id = myRef.push().getKey();
            HashMap<Object,String> data = new HashMap<>();
            data.put("Alasan", alasan);
            myRef.child(id).setValue(data);
        }
    }
}
