package com.example.nurilmiadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Bab extends AppCompatActivity {
    String description = "penambahan bab baru";
    public String bab;
    EditText et_bab;
    Button btn;
    RecyclerView recyclerView;

    FirebaseDatabase database;
    DatabaseReference myref;
    List<BabModel> babModeLList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bab);

        //---------------inisiasi firebase
        database = FirebaseDatabase.getInstance();

        //---------------getdata
        babModeLList = new ArrayList<>();
        myref = database.getReference("bab");
        myref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (snapshot.getChildrenCount() == 3) {
                        BabModel babModel = snapshot.getValue(BabModel.class);
                        babModeLList.add(babModel);
                        Log.e("snapshot: ", snapshot.getValue().toString());
                    }
                }
                showDataBab();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        et_bab = findViewById(R.id.editText);
        recyclerView = findViewById(R.id.rec);

        btn = findViewById(R.id.button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bab = et_bab.getText().toString();
                Intent intent = new Intent(Bab.this, Deskripsi.class);
                intent.putExtra("bab", bab);
                startActivity(intent);
                finish();
            }
        });

    }
    private void showDataBab(){
        recyclerView.removeAllViews();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        BabAdapter adapter = new BabAdapter(this,babModeLList);
        recyclerView.setAdapter(adapter);





    }}

