package com.example.nurilmiadmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BabAdapter extends RecyclerView.Adapter<BabAdapter.ViewHolder> {

    Context context;
    List<BabModel> babModelList;

    public BabAdapter(Context context, List<BabModel> babModelList) {
        this.context = context;
        this.babModelList = babModelList;
    }

    @NonNull
    @Override
    public BabAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_bab, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BabAdapter.ViewHolder holder, int position) {
        holder.textViewJudul.setText(babModelList.get(position).getJudulbab());
    }

    @Override
    public int getItemCount() {
        return babModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewJudul;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewJudul = itemView.findViewById(R.id.textviewJudulBab);
        }
    }
}
