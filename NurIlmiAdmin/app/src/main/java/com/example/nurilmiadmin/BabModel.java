package com.example.nurilmiadmin;

public class BabModel {
   String id;
   String judulbab;
   String deskripsi;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudulbab() {
        return judulbab;
    }

    public void setJudulbab(String judulbab) {
        this.judulbab = judulbab;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}

