package com.example.nurilmiadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class DaftarGuru extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_guru);
    }

    public void pilihan(View view) {
        TextView pilih = findViewById(R.id.textView12);
        pilih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DaftarGuru.this,
                        InputJadwal.class);
                startActivity(intent);
            }
        });

    }

    public void back(View view) {
        ImageButton guru = findViewById(R.id.imageButton3);
        guru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DaftarGuru.this,
                        MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public void home(View view) {
        ImageButton home = findViewById(R.id.imageButton);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DaftarGuru.this,
                        MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
