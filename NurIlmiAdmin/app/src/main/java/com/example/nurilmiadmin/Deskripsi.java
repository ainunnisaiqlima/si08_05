package com.example.nurilmiadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class Deskripsi extends AppCompatActivity {
    TextView tv_private;
    TextView tv_bab;
    TextView tv_deskripsi;
    EditText et_deskripsi;
    Button btn_add;
    Button btn_delete;
    FirebaseDatabase database;
    DatabaseReference myref;
    public String judulbab, deskripsi,id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deskripsi);

        tv_private = findViewById(R.id.tv_private);
        tv_bab = findViewById(R.id.tv_bab);
        tv_deskripsi = findViewById(R.id.tv_deskripsi);
        et_deskripsi = findViewById(R.id.et_deskripsi);
        btn_add = findViewById(R.id.btn_add);


        database = FirebaseDatabase.getInstance();
        myref = database.getReference("bab");

        Intent intent = getIntent();
        judulbab = intent.getStringExtra("bab");
        tv_bab.setText(judulbab);
        deskripsi = et_deskripsi.getText().toString();
    }

    public void add(View view) {

        if (deskripsi != null) {
            Nambahbab babjudul = new Nambahbab(judulbab, deskripsi);
            id = myref.push().getKey();
            HashMap<Object,String> data = new HashMap<>();
            data.put("id",id);
            data.put("judulbab",judulbab);
            data.put("deskripsi",deskripsi);
            myref.child(id).setValue(data);
            Toast.makeText(this,"Insert data success",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this,"Error adding", Toast.LENGTH_SHORT).show();
        }

}

    public void delete(View view) {

    }

    public void next(View view) {
    }

    public void ex(View view) {
        Intent c = new Intent(Deskripsi.this, Upload.class);
        c.putExtra(id, "id");
        c.putExtra(judulbab, "judulbab");
        startActivity(c);

    }
}
