package com.example.nurilmiadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class InputJadwal extends AppCompatActivity {
    EditText edt_tanggal, edt_waktu;
    FirebaseDatabase database;
    DatabaseReference myRef;
    String tanggal, waktu;
    public static final String date = "";
    public static final String time = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_jadwal);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("jadwal");
        Intent intent = getIntent();
        tanggal =intent.getStringExtra("tanggal");
        waktu = intent.getStringExtra("waktu");
        edt_tanggal = findViewById(R.id.editText2);
        edt_waktu = findViewById(R.id.editText3);
        Button btn =findViewById(R.id.button3);

    }

    public void pilihmatpel(View view) {

        String tanggal = edt_tanggal.getText().toString();
        String waktu = edt_waktu.getText().toString();
        if (!TextUtils.isEmpty(tanggal) || !TextUtils.isEmpty(waktu)){
            Toast.makeText(this,"Data Berhasil Masuk",Toast.LENGTH_SHORT).show();
            Jadwal tanggalwaktu = new Jadwal(tanggal, waktu);
            String id = myRef.push().getKey();
            HashMap<Object,String> data = new HashMap<>();
            data.put("Tanggal",tanggal);
            data.put("Waktu",waktu);
            myRef.child(id).setValue(data);

            Button pilihmatpel = findViewById(R.id.button3);
            pilihmatpel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(InputJadwal.this,
                            PilihMatpel.class);
                    intent.putExtra(date, edt_tanggal.getText().toString());
                    intent.putExtra(time, edt_waktu.getText().toString());

                    startActivity(intent);
                }
            });

        }

    }



    public void back(View view) {
        ImageButton back = findViewById(R.id.imageButton12);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InputJadwal.this,
                        DaftarGuru.class);
                startActivity(intent);
            }
        });
    }

    public void home(View view) {
        ImageButton home = findViewById(R.id.imageButton4);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InputJadwal.this,
                        MainActivity.class);
                startActivity(intent);
            }
        });
    }
    }

