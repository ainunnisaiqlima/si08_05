package com.example.nurilmiadmin;

public class Jadwal {
    private String tanggal, waktu;

    public Jadwal(String tanggal, String waktu){
        this.tanggal = tanggal;
        this.waktu = waktu;
    }
    public Jadwal(){

    }

    public String getTanggal(){ return tanggal; }

    public String getWaktu(){ return waktu; }
}
