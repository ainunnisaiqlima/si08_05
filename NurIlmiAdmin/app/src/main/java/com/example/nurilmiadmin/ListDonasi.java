package com.example.nurilmiadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

public class ListDonasi extends AppCompatActivity {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private Spinner mSpinner;
    int currentItem = 0;
    String [] pilihan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_donasi);
        mSpinner = findViewById(R.id.list_donasi);
        pilihan = getResources().getStringArray(R.array.list_donasi);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (currentItem==position){
                    return;
                }else{
                    Intent intent = new Intent(ListDonasi.this, CekListDonasi.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageView image = findViewById(R.id.user);
        image.setImageResource(R.drawable.user);
        ImageView image2 = findViewById(R.id.user2);
        image2.setImageResource(R.drawable.user);
        ImageView image3 = findViewById(R.id.user3);
        image3.setImageResource(R.drawable.user);
        ImageView image4 = findViewById(R.id.user4);
        image4.setImageResource(R.drawable.user);
        ImageView image5 = findViewById(R.id.user5);
        image5.setImageResource(R.drawable.user);
        getSupportActionBar().setTitle("List Donasi");

    }

    public void prifat(View view) {
        ImageView prifat = findViewById(R.id.Privat);
        prifat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ListDonasi.this,
                        DaftarGuru.class);
                startActivity(intent);
            }
        });
    }
    }

