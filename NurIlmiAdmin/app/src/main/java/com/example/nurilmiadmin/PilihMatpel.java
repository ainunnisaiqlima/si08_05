package com.example.nurilmiadmin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class PilihMatpel extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference myRef;
    RecyclerView recyclerView;
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    public static final String date1 = "";
    public static final String time1 = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_matpel);

        database = FirebaseDatabase.getInstance();

        myRef = database.getReference("bab");

        Intent intent = getIntent();
        String ambildata1 = intent.getStringExtra(InputJadwal.date);
        String ambildata2 = intent.getStringExtra(InputJadwal.time);
    }

    public void alasan(View view) {
        Button alasan = findViewById(R.id.button2);
        alasan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PilihMatpel.this,
                        Alasan.class);

                startActivity(intent);
            }
        });
    }

    public void back(View view) {
        ImageButton back = findViewById(R.id.imageButton01);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PilihMatpel.this,
                        InputJadwal.class);
                startActivity(intent);
            }
        });
    }

    public void home(View view) {
        ImageButton home = findViewById(R.id.imageButton0);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PilihMatpel.this,
                        MainActivity.class);
                startActivity(intent);
            }
        });
    }
    }
