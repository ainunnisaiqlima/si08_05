package com.example.nurilmiadmin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

public class Upload extends AppCompatActivity implements View.OnClickListener {

    Button btn_add, btn_upload;
    ImageView photo;
    TextView tv_judulbab;

    Uri filepath;
    int PICK_IMAGE_REQUEST = 71;
    ProgressDialog loading;
    public Uri donwload;
    public String id, judulbab;

    FirebaseDatabase database;
    DatabaseReference myref;
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseAuth mAuth;
    FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        Intent c = getIntent();
        id = c.getStringExtra("id");
        judulbab = c.getStringExtra("judulbab");

        btn_add = findViewById(R.id.btn_add);
        btn_upload = findViewById(R.id.btn_upload);
        photo = findViewById(R.id.imageView);
        tv_judulbab = findViewById(R.id.tv_judulbab);
        loading = new ProgressDialog(this);

        //db = FirebaseDatabase.getInstance().getReference("bab");

        database = FirebaseDatabase.getInstance();
        myref = database.getReference("image");

        tv_judulbab.setText(judulbab);
        Toast.makeText(this, "ID sebelumya: "+id+"", Toast.LENGTH_LONG);

        /*
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select image"), IMAGE_REQUEST_CODE);
            }
        });
         */

        btn_add.setOnClickListener(this);
        btn_upload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_upload:
                uploadImage();
                loading.setTitle("Uploading....");
                loading.show();
                break;
            case R.id.btn_add:
                Intent galery = new Intent();
                galery.setType("image/*");
                galery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(galery,"Select Photo"),PICK_IMAGE_REQUEST);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && RESULT_OK == resultCode&& data != null && data.getData() != null){
            filepath = data.getData();
            try {
                Bitmap photo_bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                photo.setImageBitmap(photo_bmp);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public  void uploadImage(){
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        if (filepath != null) {
            String filename = "images/"+ myref.push().getKey();
            StorageReference ref = storageReference.child(filename);
            ref.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!uriTask.isSuccessful()) ;
                    donwload = uriTask.getResult();
                    String id_img = myref.push().getKey();
                    HashMap<Object, String> data = new HashMap<>();
                    data.put("id_img", id_img);
                    data.put("image", donwload.toString());
                    myref.child(id).setValue(data);
                    Toast.makeText(Upload.this, "Berhasil", Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        }else{
            Toast.makeText(this, "error upload foto", Toast.LENGTH_SHORT).show();
        }
    }
}